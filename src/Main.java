import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
//        mojCzas();
//        birthdaysDaysOfWeek();

        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//        System.out.println(changeDateToLocalDate(date));
        System.out.println(localDate);
        Date date2 = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        System.out.println(date2);


    }

    private static LocalDate changeDateToLocalDate(Date date) {
        String[] arg;
        String string = date.toString();
        arg = string.split(" ");
        return LocalDate.of(Integer.parseInt(arg[5]), month(arg[1]), Integer.parseInt(arg[2]));
    }

    private static int month(String s) {
        switch (s) {
            case "Oct":
                return 10;
            default:
                return 1;
        }
    }

    private static void birthdaysDaysOfWeek() {
        LocalDate localDate = LocalDate.parse("2017-10-21");
        for (int i = 0; i <= 10; i++) {
            System.out.println("Urodziny przez w roku: " + localDate.plusYears(i).getYear() + " : " + localDate.plusYears(i).getDayOfWeek());
        }
    }

    private static void mojCzas() {
        LocalDateTime initialDate = LocalDateTime.parse("1971-10-16T00:00:00");
        LocalDateTime now = LocalDateTime.now();
        LocalDate initialDate2 = LocalDate.parse("1971-10-16");
        LocalDate now2 = LocalDate.now();

        System.out.println("Seconds: " + Duration.between(initialDate, now).getSeconds());
        System.out.println("Minutes: " + ChronoUnit.MINUTES.between(initialDate, now));
        System.out.println("Days: " + ChronoUnit.DAYS.between(initialDate, now));
        System.out.println("Weeks: " + ChronoUnit.WEEKS.between(initialDate, now));
        System.out.println("Months: " + ChronoUnit.MONTHS.between(initialDate, now));
        System.out.println("LeapYears: " + leapYearsNumber(initialDate2, now2));


    }

    private static int leapYearsNumber(LocalDate initialDate2, LocalDate now2) {
        int counter = 0;
        long j = 0;
        for (int i = initialDate2.getYear(); i < now2.getYear(); i++) {
            if (initialDate2.plusYears(j).isLeapYear()) {
                counter++;
            }
            j++;
        }
        return counter;
    }
}
